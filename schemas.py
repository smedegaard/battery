import typing


class BatteryData(typing.NamedTuple):
    cycle_index: int
    discharge_capacity: float
    charge_capacity: float
    discharge_energy: float
    charge_energy: float
    dc_internal_resistance: float
    temperature_average: float
    energy_efficiency: float
    charge_throughput: float
    energy_throughput: float
    charge_duration: float
