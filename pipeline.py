import os
import argparse
import logging
import re

from typing import Tuple, Iterable

import pendulum

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions

from apache_beam.io.textio import ReadFromText

from apache_beam.io import fileio

from apache_beam.options.pipeline_options import SetupOptions

from transforms.par_dos import InputFilesToDicts

from apache_beam.dataframe.convert import to_dataframe

import schemas


def run(
    pipeline_args: dict,
    input_dir: str,
    # num_shards: int,
) -> None:

    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True

    with beam.Pipeline(options=pipeline_options) as pipeline:
        json_path = os.path.join(input_dir, "*.json")

        summary_dict = (
            pipeline
            | "Match data files" >> fileio.MatchFiles(json_path)
            | "Read files" >> fileio.ReadMatches()
            | "Reshuffle" >> beam.Reshuffle()
            | "Get Content" >> beam.Map(lambda x: x.read_utf8())
            | "Convert To Dicts"
            >> beam.ParDo(InputFilesToDicts().with_output_types(schemas.BatteryData))
            | "Write to files"
            >> beam.io.WriteToText("./results", file_name_suffix=".jso")
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input_dir",
        type=str,
        dest="input_dir",
        default="/Users/anders/skunk/battery/data",
        help="Input path to read the csv files from.",
    )

    # parser.add_argument("--runner", default="InteractiveRunner", dest="runner")
    parser.add_argument("--runner", default="DirectRunner", dest="runner")

    known_args, pipeline_args = parser.parse_known_args()
    job_name = f"battery-third-rock-{pendulum.now().to_iso8601_string()}"
    pipeline_args.extend(
        [
            f"--runner={known_args.runner}",
            # "--project=battery-third-rock",
            # "--region=europe-north1",
            # "--staging_location=gs://battery-third-rock-data/staging/",
            # "--temp_location=gs://battery-third-rock-data/temp/",
            f"--job_name={job_name}",
            # "--setup_file=./setup.py",
        ]
    )

    print("\n------------------------")
    print(f"JOB NAME: {job_name}")
    #   print(f"WRITING TO: {known_args.output_dir}")
    print("------------------------")

    run(
        pipeline_args,
        input_dir=known_args.input_dir,
        # num_shards=known_args.num_shards,
    )
