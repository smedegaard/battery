import os
import sys
import json

from typing import Dict

import apache_beam as beam
from apache_beam.dataframe.convert import to_dataframe


class InputFilesToDicts(beam.DoFn):
    def process(self, input):
        obj = json.loads(input)
        data = obj["summary"]
        drop_cols = [
            "date_time_iso",
            "paused",
            "temperature_maximum",
            "temperature_minimum",
            "time_temperature_integrated",
            "charge_duration",
        ]
        for c in drop_cols:
            del data[c]

        yield data


# class CleanData(beam.DoFn):
#     def process(self, input):
#         df = to_dataframe(input)
#         yield df
